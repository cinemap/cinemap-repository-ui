<!--
  ~ Copyright 2024 Benjamin Dittwald
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<!DOCTYPE html>
<html lang="en" data-bs-theme="dark">
<div th:if="${operation.equals('edit')}"
     th:replace="~{fragments/head :: head (title='Cinemap Repository UI - ' + #{movie_form.meta.page.title.edit})}"></div>
<div th:if="${operation.equals('add')}"
     th:replace="~{fragments/head :: head (title='Cinemap Repository UI - ' + #{movie_form.meta.page.title.add})}"></div>
<body>

<div th:replace="~{fragments/navbar :: navbar}"></div>

<!-- Toasts for added films -->
<div th:if="${toast_state != null && operation.equals('add')}">
    <div class="toast-container position-static top-0 end-0 z-3 position-absolute mt-4 mx-4">
        <div th:each="toast : ${toast_state}" class="mb-2">
            <div th:switch="${toast}">
                <div th:case="${T(de.dittwald.cinemap.repositoryui.config.ToastStates).ERROR_MOVIE_ADD.name}">
                    <div th:replace="~{fragments/toast :: toast (toastStyle='text-bg-danger', title=#{toast.error.add.movie.title}, message=#{toast.error.add.movie.text})}"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Toasts for edited films -->
<div th:if="${toast_state != null && operation.equals('edit')}">
    <div class="toast-container position-static top-0 end-0 z-3 position-absolute mt-4 mx-4">
        <div th:each="toast : ${toast_state}" class="mb-2">
            <div th:switch="${toast}">
                <div th:case="${T(de.dittwald.cinemap.repositoryui.config.ToastStates).ERROR_MOVIE_UPDATE.name}">
                    <div th:replace="~{fragments/toast :: toast (toastStyle='text-bg-danger', title=#{toast.error.update.movie.title}, message=#{toast.error.update.movie.text})}"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row mt-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        class="link-primary link-underline-opacity-25 link-underline-opacity-100-hover" href="/"
                        th:text="#{breadcrumb.home}">Home</a></li>
                <li class="breadcrumb-item"><a
                        class="link-primary link-underline-opacity-25 link-underline-opacity-100-hover" href="/movies"
                        th:text="#{breadcrumb.movies}">Movies</a></li>
                <li th:if="${operation.equals('edit')}" class="breadcrumb-item active" aria-current="page"><a
                        class="link-primary link-underline-opacity-25 link-underline-opacity-100-hover"
                        th:href="'/movies/' + ${movie.uuid} + '/scenes'"
                        th:text="${movie.title}">Movie title
                    title</a>
                </li>
                <li th:if="${operation.equals('edit')}" class="breadcrumb-item active" aria-current="page"
                    th:text="#{breadcrumb.update_movie}">Movie edit
                </li>
                <li th:if="${operation.equals('add')}" class="breadcrumb-item active" aria-current="page"
                    th:text="#{breadcrumb.add_movie}">Add Movie
                </li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <h1 th:if="${operation.equals('edit')}"><span th:text="#{movie_form.heading.edit_movie}">Edit movie</span></h1>
        <h1 th:if="${operation.equals('add')}"><span th:text="#{movie_form.heading.add_movie}">Add movie</span></h1>
    </div>
</div>

<div class="container">
    <div class="row mt-5">
        <div class="col-sm-12 col-lg-8">
            <form class="mt-3 mb-5"
                  th:action="@{/movies/{operation}(operation=${operation})}"
                  th:object="${movie}"
                  method="post">
                <div class="d-none">
                    <input type="text" aria-label="input-uuid" th:field="*{uuid}">
                    <input type="text" aria-label="input-locale" th:field="*{locale}">
                </div>
                <div class="row g-3">
                    <div class="col-12">
                        <label for="input-movie-title" class="form-label"
                               th:text="#{movie_form.form.input.label.title}"></label>
                        <input id="input-movie-title" type="text" class="form-control" th:field="*{title}"
                               aria-label="title"
                               aria-describedby="input-movie-title"
                               th:classappend="${#fields.hasErrors('title')} ? 'is-invalid' : ''">
                        <div class="invalid-feedback" th:if="${#fields.hasErrors('title')}" th:errors="*{title}">
                            Invalid
                        </div>
                    </div>
                    <div class="col-12">
                        <label for="textarea-movie-description" class="form-label"
                               th:text="#{movie_form.form.input.label.description}"></label>
                        <textarea id="textarea-movie-description" class="form-control" aria-label="description"
                                  th:field="*{overview}"
                                  th:classappend="${#fields.hasErrors('overview')} ? 'is-invalid' : ''"></textarea>
                        <div class="invalid-feedback" th:if="${#fields.hasErrors('overview')}"
                             th:errors="*{overview}">Invalid
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-10">
                        <label for="input-movie-tagline" class="form-label"
                               th:text="#{movie_form.form.input.label.tagline}"></label>
                        <input id="input-movie-tagline" type="text" class="form-control" th:field="*{tagline}"
                               aria-label="tagline"
                               aria-describedby="input-movie-tagline"
                               th:classappend="${#fields.hasErrors('tagline')} ? 'is-invalid' : ''">
                        <div class="invalid-feedback" th:if="${#fields.hasErrors('tagline')}" th:errors="*{tagline}">
                            Invalid
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-2">
                        <label for="input-movie-release-year" class="form-label"
                               th:text="#{movie_form.form.input.label.release_year}"></label>
                        <div class="input-group has-validation mb-3">
                            <span class="input-group-text" id="image-movie-poster-url"><i
                                    class="bi bi-calendar-check-fill"></i></span>
                            <input id="input-movie-release-year" type="text" aria-label="input-lon" class="form-control"
                                   th:field="*{releaseYear}"
                                   th:classappend="${#fields.hasErrors('releaseYear')} ? 'is-invalid' : ''">
                            <div class="invalid-feedback" th:if="${#fields.hasErrors('releaseYear')}"
                                 th:errors="*{releaseYear}">Invalid
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <label for="input-movie-tagline" class="form-label"
                               th:text="#{movie_form.form.input.label.poster_url}"></label>
                        <div class="input-group has-validation mb-3">
                            <span class="input-group-text" id="image-movie-tagline"><i
                                    class="bi bi-image-fill"></i></span>
                            <input id="input-movie-poster-url" type="text" class="form-control" th:field="*{posterUrl}"
                                   aria-label="poster-url"
                                   aria-describedby="input-movie-poster-url"
                                   th:classappend="${#fields.hasErrors('posterUrl')} ? 'is-invalid' : ''">
                            <div class="invalid-feedback" th:if="${#fields.hasErrors('posterUrl')}"
                                 th:errors="*{posterUrl}">
                                Invalid
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-5">
                        <label for="input-movie-imdb-id" class="form-label"
                               th:text="#{movie_form.form.input.label.imdb_id}"></label>
                        <input id="input-movie-imdb-id" type="text" aria-label="input-lat" class="form-control"
                               th:field="*{imdbId}" th:classappend="${#fields.hasErrors('imdbId')} ? 'is-invalid' : ''">
                        <div class="invalid-feedback" th:if="${#fields.hasErrors('imdbId')}" th:errors="*{imdbId}">
                            Invalid
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-5">
                        <label for="input-movie-tmdb-id" class="form-label"
                               th:text="#{movie_form.form.input.label.tmdb_id}"></label>
                        <input id="input-movie-tmdb-id" type="text" aria-label="input-lat" class="form-control"
                               th:field="*{tmdbId}" th:classappend="${#fields.hasErrors('tmdbId')} ? 'is-invalid' : ''">
                        <div class="invalid-feedback" th:if="${#fields.hasErrors('tmdbId')}" th:errors="*{tmdbId}">
                            Invalid
                        </div>
                    </div>
                    <div class="col-12">
                        <a th:href="'/movies'" class="btn btn-secondary"
                           th:text="#{common.button.label.cancel}">Cancel
                        </a>
                        <button th:if="${operation.equals('edit')}" type="submit" class="btn btn-primary mx-2"
                                id="edit-movie-submit"><span
                                th:text="#{movie_form.form.button.edit_movie}"></span></button>
                        <button th:if="${operation.equals('add')}" type="submit" class="btn btn-primary mx-2"
                                id="add-movie-submit"><span
                                th:text="#{movie_form.form.button.add_movie}"></span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<div th:replace="~{fragments/footer :: footer}"></div>
<script type="text/javascript"
        th:src="@{/js/bootstrap.bundle.min.js}"></script>
<script type="text/javascript" th:src="@{/js/script.js}"></script>
</body>
</html>