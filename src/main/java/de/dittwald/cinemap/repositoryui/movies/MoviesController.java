/*
 * Copyright 2024 Benjamin Dittwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dittwald.cinemap.repositoryui.movies;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.dittwald.cinemap.repositoryui.config.Constants;
import de.dittwald.cinemap.repositoryui.repository.RepositoryClient;
import de.dittwald.cinemap.repositoryui.repository.RepositoryResponseException;
import de.dittwald.cinemap.repositoryui.config.ToastStates;
import de.dittwald.cinemap.repositoryui.tmdb.TmdbId;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/movies")
@Slf4j
public class MoviesController {

    private final RepositoryClient repositoryClient;

    public MoviesController(RepositoryClient repositoryClient) {
        this.repositoryClient = repositoryClient;
    }

    @GetMapping
    public String index(Model model, @ModelAttribute(Constants.TOAST_STATE) String toastState)
            throws JsonProcessingException {

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        try {
            List<MovieFlat> movies = new ArrayList<>(this.repositoryClient.getAllMovies());
            Collections.sort(movies);
            model.addAttribute(Constants.MOVIES, movies);
        } catch (WebClientRequestException | RepositoryResponseException e) {
            model.addAttribute(Constants.MOVIES, Collections.emptyList());
            toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
            log.error(e.getMessage());
        }
        model.addAttribute(Constants.TOAST_STATE, toasts);
        model.addAttribute("tmdbId", new TmdbId());

        return "movies";
    }

    @PostMapping("/delete/{movieUuid}")
    public String deleteMovie(@PathVariable UUID movieUuid, RedirectAttributes redirectAttributes) {

        try {
            this.repositoryClient.deleteMovie(movieUuid);
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, ToastStates.SUCCESS_MOVIE_DELETE.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, ToastStates.ERROR_MOVIE_DELETE.name());
            log.error(e.getMessage());
        }

        return "redirect:/movies";
    }

    @PostMapping
    public String createMovieByTmdbId(@Valid @ModelAttribute TmdbId tmdbId, BindingResult result, Model model,
                                      RedirectAttributes redirectAttributes,
                                      @ModelAttribute(Constants.TOAST_STATE) String toastState) {

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        if (result.hasErrors()) {
            try {
                List<MovieFlat> movies = new ArrayList<>(this.repositoryClient.getAllMovies());
                Collections.sort(movies);
                model.addAttribute(Constants.MOVIES, movies);
                toasts.add(toastState);
            } catch (WebClientRequestException | RepositoryResponseException e) {
                model.addAttribute(Constants.MOVIES, Collections.emptyList());
                toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
                log.error(e.getMessage());
            }

            model.addAttribute(Constants.TOAST_STATE, toasts);

            return "movies";
        }

        try {
            this.repositoryClient.createMovieViaTmdbId(tmdbId.getId());
            toasts.add(ToastStates.SUCCESS_MOVIE_ADD.name());
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, ToastStates.SUCCESS_MOVIE_ADD.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            toasts.add(ToastStates.ERROR_MOVIE_ADD.name());
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, ToastStates.ERROR_MOVIE_ADD.name());
            log.error(e.getMessage());
        }

        return "redirect:/movies";
    }

    @PostMapping("/add")
    public String addMovie(@Valid @ModelAttribute(name = "movie") MovieFlat movie, BindingResult result, Model model,
                           @ModelAttribute(Constants.TOAST_STATE) String toastState,
                           RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            model.addAttribute(Constants.OPERATION, Constants.ADD);
            return "movie_form";
        }

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        try {
            this.repositoryClient.createMovie(movie);
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, ToastStates.SUCCESS_MOVIE_ADD.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            model.addAttribute(Constants.OPERATION, Constants.ADD);
            toasts.add(ToastStates.ERROR_MOVIE_ADD.name());
            model.addAttribute(Constants.TOAST_STATE, toasts);
            log.error(e.getMessage());
            return "movie_form";
        }

        return "redirect:/movies/%s/scenes".formatted(movie.getUuid());
    }

    @PostMapping("/edit")
    public String editMovie(@Valid @ModelAttribute(name = "movie") MovieFlat movie, BindingResult result, Model model,
                            @ModelAttribute(Constants.TOAST_STATE) String toastState,
                            RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            model.addAttribute(Constants.OPERATION, Constants.EDIT);
            return "movie_form";
        }

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        try {
            this.repositoryClient.updateMovie(movie);
            toasts.add(ToastStates.SUCCESS_MOVIE_UPDATE.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            model.addAttribute(Constants.OPERATION, Constants.EDIT);
            toasts.add(ToastStates.ERROR_MOVIE_UPDATE.name());
            model.addAttribute(Constants.TOAST_STATE, toasts);
            log.error(e.getMessage());
            return "movie_form";
        }

        redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);

        return "redirect:/movies/%s/scenes".formatted(movie.getUuid());
    }

    @GetMapping("/form")
    public String showMovieForm(@RequestParam(name = "movieUuid", required = false) UUID movieUuid, Model model) {

        MovieFlat movie;
        if (movieUuid == null) {
            movie = new MovieFlat();
            movie.setUuid(UUID.randomUUID());
            movie.setLocale(LocaleContextHolder.getLocale().getLanguage());
            model.addAttribute("operation", "add");
        } else {
            movie = this.repositoryClient.getMovie(movieUuid);
            model.addAttribute("operation", "edit");
        }

        model.addAttribute("movie", movie);

        return "movie_form";
    }
}
