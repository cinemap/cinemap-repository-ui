/*
 * Copyright 2024 Benjamin Dittwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dittwald.cinemap.repositoryui.scenes;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.dittwald.cinemap.repositoryui.config.Constants;
import de.dittwald.cinemap.repositoryui.config.ToastStates;
import de.dittwald.cinemap.repositoryui.repository.RepositoryClient;
import de.dittwald.cinemap.repositoryui.repository.RepositoryResponseException;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/movies")
@Slf4j
public class ScenesController {

    private final RepositoryClient repositoryClient;

    public ScenesController(RepositoryClient repositoryClient) {
        this.repositoryClient = repositoryClient;
    }

    @GetMapping("{movieUuid}/scenes")
    public String showScenesListPage(@PathVariable("movieUuid") UUID movieUuid, Model model,
                                     @ModelAttribute(Constants.TOAST_STATE) String toastState) {

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        try {
            model.addAttribute(Constants.MOVIE, this.repositoryClient.getMovie(movieUuid));
            model.addAttribute(Constants.SCENES, this.repositoryClient.getScenesForMovie(movieUuid));
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_RETRIEVAL.name());
            if (model.containsAttribute(Constants.MOVIE)) {
                model.addAttribute(Constants.SCENES, Collections.emptyList());
            }
        }

        model.addAttribute(Constants.TOAST_STATE, toasts);

        return "scenes";
    }

    @GetMapping("{movieUuid}/scenes/{sceneUuid}")
    public String showSceneEditPage(@PathVariable("movieUuid") UUID movieUuid,
                                    @PathVariable("sceneUuid") UUID sceneUuid, Model model,
                                    @ModelAttribute(Constants.TOAST_STATE) String toastState) {

        List<String> toasts = new ArrayList<>();

        if (toastState != null && !toastState.isEmpty()) {
            toasts.add(toastState);
        }

        try {
            model.addAttribute(Constants.MOVIE, this.repositoryClient.getMovie(movieUuid));
            model.addAttribute(Constants.SCENE, this.repositoryClient.getScene(sceneUuid, movieUuid));
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_RETRIEVAL.name());
            if (model.containsAttribute(Constants.MOVIE)) {
                model.addAttribute(Constants.SCENES, Collections.emptyList());
            }
        }

        model.addAttribute(Constants.TOAST_STATE, toasts);

        return "scene_form";
    }

    @PostMapping("{movieUuid}/scenes/add")
    public String addScene(@PathVariable("movieUuid") UUID movieUuid, @Valid @ModelAttribute Scene scene,
                           BindingResult result, Model model, RedirectAttributes redirectAttributes)
            throws JsonProcessingException {

        List<String> toasts = new ArrayList<>();

        if (result.hasErrors()) {
            model.addAttribute(Constants.OPERATION, Constants.ADD);
            try {
                model.addAttribute(Constants.MOVIE, this.repositoryClient.getMovie(movieUuid));
            } catch (WebClientRequestException | RepositoryResponseException e) {
                log.error(e.getMessage());
                toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
                redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);

                // Go to movie page and display empty page with error toast if movie cannot be retrieved
                return "redirect:/movies";
            }
            return "scene_form";
        }

        try {
            this.repositoryClient.createScene(scene, movieUuid);
            toasts.add(ToastStates.SUCCESS_SCENE_ADD.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_ADD.name());
        }

        redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);

        return "redirect:/movies/%s/scenes".formatted(movieUuid);
    }

    @PostMapping("{movieUuid}/scenes/edit")
    public String editScene(@PathVariable("movieUuid") UUID movieUuid, @Valid @ModelAttribute Scene scene,
                            BindingResult result, Model model, RedirectAttributes redirectAttributes) {

        List<String> toasts = new ArrayList<>();

        if (result.hasErrors()) {
            model.addAttribute(Constants.OPERATION, Constants.EDIT);
            try {
                model.addAttribute(Constants.MOVIE, this.repositoryClient.getMovie(movieUuid));
            } catch (WebClientRequestException | RepositoryResponseException e) {
                log.error(e.getMessage());
                toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
                redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);
                // Go to movie page and display empty page with error toast if movie cannot be retrieved
                return "redirect:/movies";
            }
            return "scene_form";
        }

        try {
            this.repositoryClient.updateScene(movieUuid, scene);
            toasts.add(ToastStates.SUCCESS_SCENE_UPDATE.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_UPDATE.name());
        }

        redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);

        return "redirect:/movies/%s/scenes".formatted(movieUuid);
    }

    @GetMapping("{movieUuid}/scenes/form")
    public String showSceneForm(@PathVariable("movieUuid") UUID movieUuid,
                                @RequestParam(name = "sceneUuid", required = false) UUID sceneUuid, Model model,
                                RedirectAttributes redirectAttributes) {

        List<String> toasts = new ArrayList<>();

        Scene scene;
        if (sceneUuid == null) {
            scene = new Scene();
            scene.setUuid(UUID.randomUUID());
            scene.setLocale(LocaleContextHolder.getLocale().getLanguage());
            model.addAttribute(Constants.OPERATION, Constants.ADD);
        } else {
            try {
                scene = this.repositoryClient.getScene(sceneUuid, movieUuid);
                model.addAttribute(Constants.OPERATION, Constants.EDIT);
            } catch (WebClientRequestException | RepositoryResponseException e) {
                log.error(e.getMessage());
                toasts.add(ToastStates.ERROR_SCENE_RETRIEVAL.name());
                redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);
                return "redirect:/movies";
            }
        }

        model.addAttribute(Constants.SCENE, scene);

        try {
            model.addAttribute(Constants.MOVIE, this.repositoryClient.getMovie(movieUuid));
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
            redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);
            return "redirect:/movies";
        }

        return "scene_form";
    }

    @PostMapping("{movieUuid}/scenes/{sceneUuid}")
    public String deleteScene(@PathVariable("movieUuid") UUID movieUuid, @PathVariable("sceneUuid") UUID sceneUuid,
                              RedirectAttributes redirectAttributes) {
        List<String> toasts = new ArrayList<>();

        try {
            this.repositoryClient.deleteScene(sceneUuid, movieUuid);
            toasts.add(ToastStates.SUCCESS_SCENE_DELETE.name());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_DELETE.name());
        }

        redirectAttributes.addFlashAttribute(Constants.TOAST_STATE, toasts);

        return "redirect:/movies/%s/scenes".formatted(movieUuid);
    }
}
