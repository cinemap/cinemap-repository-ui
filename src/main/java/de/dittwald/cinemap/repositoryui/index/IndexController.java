package de.dittwald.cinemap.repositoryui.index;

import de.dittwald.cinemap.repositoryui.config.Constants;
import de.dittwald.cinemap.repositoryui.config.ToastStates;
import de.dittwald.cinemap.repositoryui.repository.RepositoryClient;
import de.dittwald.cinemap.repositoryui.repository.RepositoryResponseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.reactive.function.client.WebClientRequestException;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
@Slf4j
public class IndexController {

    private final RepositoryClient repositoryClient;

    public IndexController(RepositoryClient repositoryClient) {
        this.repositoryClient = repositoryClient;
    }

    @GetMapping
    public String showIndex(Model model) {

        List<String> toasts = new ArrayList<>();

        try {
            model.addAttribute("moviesCount", this.repositoryClient.getAllMovies().size());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_MOVIE_RETRIEVAL.name());
        }

        try {
            model.addAttribute("scenesCount", this.repositoryClient.getAllScenes().size());
        } catch (WebClientRequestException | RepositoryResponseException e) {
            log.error(e.getMessage());
            toasts.add(ToastStates.ERROR_SCENE_RETRIEVAL.name());
        }

        model.addAttribute(Constants.TOAST_STATE, toasts);

        return "index";
    }
}
