package de.dittwald.cinemap.repositoryui.error;

import de.dittwald.cinemap.repositoryui.config.Properties;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CinemapErrorController implements ErrorController {

    private final Properties properties;

    private final MessageSource messageSource;

    public CinemapErrorController(Properties properties, MessageSource messageSource) {
        this.properties = properties;
        this.messageSource = messageSource;
    }

    @RequestMapping("/error")
    public String handlerError(HttpServletRequest request, Model model) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                model.addAttribute("errorTitle", messageSource.getMessage("error.404.meta.page.title", null, request.getLocale()));
                model.addAttribute("errorExplanation", messageSource.getMessage("error.404.error.explanation", null, request.getLocale()));
                model.addAttribute("errorQuoteText", messageSource.getMessage("error.404.quote.text", null, request.getLocale()));
                model.addAttribute("errorQuotePerson", messageSource.getMessage("error.404.quote.person", null, request.getLocale()));
                model.addAttribute("errorQuoteMovie", messageSource.getMessage("error.404.quote.movie", null, request.getLocale()));
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                model.addAttribute("errorTitle", messageSource.getMessage("error.500.meta.page.title", null, request.getLocale()));
                model.addAttribute("errorExplanation", messageSource.getMessage("error.500.error.explanation", null, request.getLocale()));
                model.addAttribute("errorQuoteText", messageSource.getMessage("error.500.quote.text", null, request.getLocale()));
                model.addAttribute("errorQuotePerson", messageSource.getMessage("error.500.quote.person", null, request.getLocale()));
                model.addAttribute("errorQuoteMovie", messageSource.getMessage("error.500.quote.movie", null, request.getLocale()));
            }
            else if(statusCode == HttpStatus.GATEWAY_TIMEOUT.value()) {
                model.addAttribute("errorTitle", messageSource.getMessage("error.504.meta.page.title", null, request.getLocale()));
                model.addAttribute("errorExplanation", messageSource.getMessage("error.504.error.explanation", null, request.getLocale()));
                model.addAttribute("errorQuoteText", messageSource.getMessage("error.504.quote.text", null, request.getLocale()));
                model.addAttribute("errorQuotePerson", messageSource.getMessage("error.504.quote.person", null, request.getLocale()));
                model.addAttribute("errorQuoteMovie", messageSource.getMessage("error.504.quote.movie", null, request.getLocale()));
            } else {
                model.addAttribute("errorTitle", messageSource.getMessage("error.fallback.meta.page.title", null, request.getLocale()));
                model.addAttribute("errorExplanation", messageSource.getMessage("error.fallback.error.explanation", null, request.getLocale()));
                model.addAttribute("errorQuoteText", messageSource.getMessage("error.fallback.quote.text", null, request.getLocale()));
                model.addAttribute("errorQuotePerson", messageSource.getMessage("error.fallback.quote.person", null, request.getLocale()));
                model.addAttribute("errorQuoteMovie", messageSource.getMessage("error.fallback.quote.movie", null, request.getLocale()));
            }
        }

        model.addAttribute("errorException", request.getAttribute(RequestDispatcher.ERROR_EXCEPTION));
        model.addAttribute("errorMessage", request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
        model.addAttribute("stagingEnvironment", properties.getStagingEnvironment());

        return "error";
    }
}
