package de.dittwald.cinemap.repositoryui.config;

public final class Constants {

    final public static String TOAST_STATE = "toast_state";
    final public static String MOVIES = "movies";
    final public static String MOVIE = "movie";
    final public static String SCENES = "scenes";
    final public static String SCENE = "scene";
    final public static String OPERATION = "operation";
    final public static String ADD = "add";
    final public static String EDIT = "edit";
}
