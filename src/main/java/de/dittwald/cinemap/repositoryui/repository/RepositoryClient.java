/*
 * Copyright 2024 Benjamin Dittwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dittwald.cinemap.repositoryui.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.dittwald.cinemap.repositoryui.movies.*;
import de.dittwald.cinemap.repositoryui.scenes.Scene;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class RepositoryClient {

    private final WebClientConfig webClientConfig;

    public RepositoryClient(WebClientConfig webClientConfig) {
        this.webClientConfig = webClientConfig;
    }

    // Todo: make reactive
    public List<MovieFlat> getAllMovies() {

        ObjectMapper objectMapper = new ObjectMapper();
        List<MovieFlat> movies = new ArrayList<>();

        try {
            JsonNode moviesNode = objectMapper.readTree(this.webClientConfig.repositoryWebClient()
                    .get()
                    .uri("/api/v1/movies?lang=" + LocaleContextHolder.getLocale().getLanguage())
                    .retrieve()
                    .onStatus(HttpStatusCode::isError,
                            response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                    .bodyToMono(String.class)
                    .block());

            for (JsonNode movieNode : moviesNode) {
                movies.add(objectMapper.treeToValue(movieNode, MovieFlat.class));
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return movies;
    }

    // Todo: make reactive
    public List<Scene> getAllScenes() {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Scene> scenes = new ArrayList<>();

        try {
            JsonNode scenesNode = objectMapper.readTree(this.webClientConfig.repositoryWebClient()
                    .get()
                    .uri("/api/v1/scenes?lang=" + LocaleContextHolder.getLocale().getLanguage())
                    .retrieve()
                    .onStatus(HttpStatusCode::isError,
                            response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                    .bodyToMono(String.class)
                    .block());

            for (JsonNode sceneNode : scenesNode) {
                scenes.add(objectMapper.treeToValue(sceneNode, Scene.class));
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return scenes;
    }

    // Todo: make reactive
    public MovieFlat getMovie(UUID movieUuid) {
        return this.webClientConfig.repositoryWebClient()
                .get()
                .uri("/api/v1/movies/%s?lang=%s".formatted(movieUuid, LocaleContextHolder.getLocale().getLanguage()))
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .bodyToMono(MovieFlat.class)
                .block();
    }

    // Todo: make reactive
    public void deleteMovie(UUID movieUuid) {
        this.webClientConfig.repositoryWebClient()
                .delete()
                .uri(String.format("/api/v1/movies/%s", movieUuid))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    // Todo: make reactive
    public void createScene(Scene scene, UUID movieUuid) {
        this.webClientConfig.repositoryWebClient()
                .post()
                .uri(String.format("/api/v1/movies/%s/scenes", movieUuid))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(scene), Scene.class)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    // Todo: make reactive
    public List<Scene> getScenesForMovie(UUID movieUuid) {

        ObjectMapper objectMapper = new ObjectMapper();
        List<Scene> scenes = new ArrayList<>();

        try {
            JsonNode scenesNode = objectMapper.readTree(this.webClientConfig.repositoryWebClient()
                    .get()
                    .uri("/api/v1/movies/%s/scenes?lang=%s".formatted(movieUuid,
                            LocaleContextHolder.getLocale().getLanguage()))
                    .retrieve()
                    .onStatus(HttpStatusCode::isError,
                            response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                    .bodyToMono(String.class)
                    .block());

            for (JsonNode sceneNoce : scenesNode) {
                scenes.add(objectMapper.treeToValue(sceneNoce, Scene.class));
            }

            return scenes;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    // Todo: make reactive
    public Scene getScene(UUID sceneUuid, UUID movieUuid) {

        return this.webClientConfig.repositoryWebClient()
                .get()
                .uri("/api/v1/movies/%s/scenes/%s?lang=%s".formatted(movieUuid, sceneUuid,
                        LocaleContextHolder.getLocale().getLanguage()))
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .bodyToMono(Scene.class)
                .block();
    }

    // Todo: make reactive
    public void createMovie(MovieFlat movie) {
        this.webClientConfig.repositoryWebClient()
                .post()
                .uri("/api/v1/movies")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(movie), MovieFlat.class)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    // Todo: make reactive
    public void updateMovie(MovieFlat movie) {
        this.webClientConfig.repositoryWebClient()
                .put()
                .uri("/api/v1/movies/%s".formatted(movie.getUuid()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(movie), MovieFlat.class)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    // Todo: make reactive
    public void updateScene(UUID movieUuid, Scene scene) {
        this.webClientConfig.repositoryWebClient()
                .put()
                .uri(String.format("/api/v1/movies/%s/scenes/%s", movieUuid, scene.getUuid()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(scene), Scene.class)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    // Todo: Make reactive
    public void deleteScene(UUID sceneUuid, UUID movieUuid) {
        this.webClientConfig.repositoryWebClient()
                .delete()
                .uri(String.format("/api/v1/movies/%s/scenes/%s", movieUuid, sceneUuid))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    public void createMovieViaTmdbId(int tmdbId) {
        this.webClientConfig.repositoryWebClient()
                .put()
                .uri(String.format("/api/v1/tmdb/%s", tmdbId))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(HttpStatusCode::isError,
                        response -> Mono.error(new RepositoryResponseException(createExceptionMessage(response))))
                .toBodilessEntity()
                .block();
    }

    private String createExceptionMessage(ClientResponse response) {
        return String.format("Retrieved status code %s with HTTP method %s on %s", response.statusCode(),
                response.request().getMethod(), response.request().getURI());
    }
}
