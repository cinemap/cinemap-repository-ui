package de.dittwald.cinemap.repositoryui.repository;

public class RepositoryResponseException extends RuntimeException {
    public RepositoryResponseException(String message) {
        super(message);
    }
}
